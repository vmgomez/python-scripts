import os
import subprocess

args_tablet = []
args_dvd_player = []
check_folder = os.getcwd() + "\\"
output_folder = "convert\\"
all_files = os.listdir(check_folder)

for one_file in all_files:
    print("[+] Converting " + one_file)
    name_without_ext = os.path.splitext(one_file)[0]

    one_file = check_folder + one_file
    output_name = check_folder + output_folder + name_without_ext + ".avi"

    #Tablet
    #flags = ["ffmpeg.exe", "-i", one_file, "-s", "800x600", "-vcodec", "mpeg4", "-acodec", "aac", "-strict", "-2", "-ac", "1", "-ar", "16000", "-r", "13", "-ab", "32000", "-aspect", "3:2", output_name]

    #Radio
    flags = ["ffmpeg.exe", "-i", one_file, "-vcodec", "libxvid", "-s", "576x432", "-b", "800k", "-r", "23.976", "-acodec", "libmp3lame", "-ac", "2", "-ar", "48000", "-ab", "128k", "-y", "-f", "avi", "-map", "0:0", "-map", "0:1", output_name]

    stdout = None
    stderr = None

    h_proc = subprocess.Popen(flags)
    (output, err) = h_proc.communicate()
    h_proc.wait()
