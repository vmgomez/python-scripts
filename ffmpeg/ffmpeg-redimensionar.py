import os
import subprocess

args_tablet = []
args_dvd_player = []
check_folder = os.getcwd() + "\\"
output_folder = "convert\\"
all_files = os.listdir(check_folder)

for one_file in all_files:
    print("[+] Converting " + one_file)
    name_without_ext = os.path.splitext(one_file)[0]

    one_file = check_folder + one_file
    output_name = check_folder + output_folder + name_without_ext + ".mkv"

    # redim
    flags = ["ffmpeg.exe", "-i", one_file, "-vf", "scale=1920:1080", output_name]

    stdout = None
    stderr = None

    h_proc = subprocess.Popen(flags)
    (output, err) = h_proc.communicate()
    h_proc.wait()
