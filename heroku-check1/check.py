import requests
import time
import json
import datetime

while True:
    print("Esperando 35mins")
    time.sleep(60*35)

    str_ret = requests.get('https://opensylar-django-test.herokuapp.com/check_fone/')
    json_obj = json.loads(str_ret.text)

    if json_obj['info'] == -6:
        print("BAN BAN! a las " + str(datetime.datetime.now()))
        print("Esperando 3H")
        time.sleep((60 * 60) * 3)

    else:
        print("Vamos bien a a las " + str(datetime.datetime.now()))
