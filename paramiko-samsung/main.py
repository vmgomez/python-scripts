import paramiko
import time
import datetime
from socket import gaierror


def ssh_loop(host, prt, usr, pwd=None, key_file=None, command=None):
    repeat = True

    while repeat:
        client = paramiko.client.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.load_system_host_keys()
        time.sleep(1)

        try:
            client.connect(host, port=prt, username=usr, password=pwd)

        # except OSError:
        #    print("[!] OSError")
        #    continue

        except gaierror:
            print("[!] gaierror")
            continue

        except ConnectionError:
            print("[!] ConnectionError")
            continue
            
        except TimeoutError:
            print("[!] TimeoutError")
            continue

        except EOFError:
            print("[!] EOFError")
            continue

        except paramiko.ssh_exception.SSHException:
            print("[!] paramiko.ssh_exception.SSHException")
            continue

        except paramiko.ssh_exception.AuthenticationException:
            print("[!] AuthenticationException ERROR")
            continue

        except paramiko.ssh_exception.NoValidConnectionsError:
            print("[!] NoValidConnectionsError ERROR")
            continue

        try:
            stdin, stdout, stderr = client.exec_command(cmd)

        except ConnectionResetError:
            print("[!] exec_command() ERROR")
            continue

        except paramiko.ssh_exception.SSHException:
            print("[!] SSHException ERROR")
            continue

        print("[+] Executing remote cmd")
        stdout = stdout.readlines()
        stderr = stderr.readlines()
        # stdin = stdin.readlines()
        client.close()

        # print(stdin)

        str_output = ''.join(str(e) for e in stdout)
        str_err_output = ''.join(str(e) for e in stderr)
        print(str_output)
        print(str_err_output)

        # print(stdout)
        #print(stderr)
        repeat = False


# cmd = "taskkill /IM googlesync.exe /f"
# cmd = "taskkill /IM explorer.exe /f"
# cmd = "taskkill /IM powershell.exe /f"
# cmd = "taskkill /IM youtube-dl.exe /f"
# cmd = "taskkill /IM JDownloader2.exe /f"
# cmd = 'powershell -c "Get-ChildItem -Path C:\ -Filter ffmpeg.exe -Recurse -ErrorAction SilentlyContinue -Force"'
# cmd = 'powershell -c "Invoke-WebRequest -Uri "https://netactuate.dl.sourceforge.net/project/gnuwin32/wget/1.11.4-1/wget-1.11.4-1-setup.exe" -UseBasicParsing'
cmd = 'powershell -c "Get-PSDrive"'
# cmd = 'powershell -c "Get-Time"'
# cmd = "taskkill /im vlc.exe /f"
# cmd = "dir C:\\Users\\opensylar\\Downloads\\"
# cmd = "dir"

# cmd = "python -V"

# cmd = 'powershell -c "ls"'
# cmd = 'python -c "import os print(os.getenv(\'PATH\'))"'
# cmd = "ping 192.168.1.111"
# cmd = 'powershell -c "New-Item -ItemType directory -Path C:\\Users\\opensylar\\Desktop\\papa\\vaqueros"'
# cmd = "C:\\Program Files\\7-Zip\\7z.exe x C:\\Users\\opensylar\\Desktop\\papa\\peliculas\\vaqueros\\*.rar -p www.megapeliculasrip.com -oC:\\Users\\opensylar\\Desktop\\papa\\peliculas\\vaqueros\\"
# cmd = 'C:\\Program Files\\Git\\usr\\bin\\find "C:\\Program Files (x86)\\" -iname "*chrome.exe"'
# cmd = 'C:\\Program Files\\Git\\usr\\bin\\du -h E:\\cmdline-tools\\sdk_android'
# cmd = 'C:\\Program Files\\Git\\usr\\bin\\du -h C:\\ProgramData\\Microsoft\\HelpLibrary2\\Catalogs\\VisualStudio15'
# cmd = 'C:\\Program Files\\Git\\usr\\bin\\du -h "C:\\Users\\opensylar\\AppData\\Local\\Freelancer Desktop App"'
# cmd = "C:\\ProgramData\\Anaconda3\\python.exe -V"
# cmd = 'powershell -c "Invoke-WebRequest -Uri https://files.pythonhosted.org/packages/7f/c0/c46eaba714630cead3713a74fc147c073fc1668786e171012316e8a652a1/ping3-2.6.5-py3-none-any.whl -OutFile ping3-2.6.5-py3-none-any.whl"'
# cmd = 'powershell -c "Get-ChildItem I:\\movibles\\ -Recurse | Remove-Item"'
# cmd = 'powershell -c "Move-Item I:\\movibles\\02-25-21 -Destination C:\\Users\\opensylar\\Desktop\\papa\\'
# cmd = 'powershell -c "Get-ChildItem I:\\movibles\\"'
# cmd = 'C:\ProgramData\Anaconda3\python.exe C:\\Users\\opensylar\\Desktop\\others\\scripts\\ping-cmd.py'
# cmd = "C:\\ProgramData\\Anaconda3\\python.exe C:\\Users\\opensylar\\Desktop\\others\\scripts\\auto-copy.py"
#cmd = "C:\\ProgramData\\Anaconda3\\python.exe C:\\Users\\opensylar\\Desktop\\others\\scripts\\patch_coookie.py"
# cmd = "del C:\\Users\\opensylar\\Desktop\\others\\scripts\\output.log"
# cmd = "taskkill /IM MicrosoftEdge.exe /f"
# cmd = "taskkill /IM chrome.exe /f"
# cmd = "tasklist"
# cmd = "C:\\Program Files\\Git\\usr\\bin\\cp -v /E/movibles/videos/otros/list /E/movibles/videos/otros/plagas/"
# cmd = "dir 'E:\\movibles'"
# cmd = "C:\ProgramData\Anaconda3\Scripts\pip.exe install psutil"
# cmd = 'C:\\Program Files\\Git\\usr\\bin\\md5sum "/C:\\Users\\opensylar\\Downloads\\Empresas_base.apk"'
# cmd = '"D:\\programfiles\\VMware\\VMware Workstation\\vmrun" -T ws start "I:\\vms\\vmware\\win10-x86\\win10-completada.vmx"'
# cmd = "dir "
# cmd = "taskkill /pid 4176 /f"
cmd = "tasklist"
#folder_name = "bb"

# folder_name = (datetime.datetime.now() + datetime.timedelta(days=6)).strftime("%m-%d-%y")
# folder_name = "mios"
# cmd = 'powershell -c "mkdir E:\\movibles\\videos\\otros\\' + folder_name + '; New-Item -ItemType "file" -Path E:\\movibles\\videos\\otros\\' + folder_name +  '\\list; Copy-Item E:\\movibles\\videos\\otros\\for-loop.ps1 -Destination E:\\movibles\\videos\\otros\\' + folder_name + '\\; Copy-Item E:\\movibles\\videos\\otros\\cookies.txt -Destination E:\\movibles\\videos\\otros\\' + folder_name + '\\'
# cmd = 'powershell -c  "Get-ChildItem -Path C:\\Users\\opensylar\\Desktop\\mega\\ -Recurse |  Move-Item -Destination I:\\movibles\\"'
# cmd = 'powershell -c  "Remove-Item -Confirm:$false C:\\Users\\opensylar\\Downloads\\boost*'
# cmd = "C:\\ProgramData\\Anaconda3\\python.exe C:/Users/opensylar/Desktop/others/scripts/ps-args-list.py"

# hst = "190.72.192.38"
# hst = "192.168.1.119"
# usr = "os-remote"
# pwd = "pxgtsjmnt"

hst = "192.168.200.222"
port = 2222
usr = "root"
private_key = "C:\\Users\\os-remote\\.ssh\\id_rsa__galaxy"

ssh_loop(hst, port, usr, key_file=private_key, command=cmd)