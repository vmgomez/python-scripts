import sqlalchemy
from concurrent import futures
from requests.exceptions import ProxyError, SSLError, ConnectionError
import requests


# data_conn = {"host": "ec2-3-230-238-86.compute-1.amazonaws.com", "user": "dqhvdohpwoeabm", "password": "428b7c20ebc859758f2b2da98fdcda1356cedfba95b466da2d19713af2bf6a04", "db" : "dd2upvfsinf9g8"}
data_conn = {"host": "127.0.0.1", "user": "user_heroku_selenium", "password": "pxgtsjmnt", "db": "heroku_selenium_db"}


def check_proxy(url, proxy, headers):
    proxy_str = proxy[0x1] + ":" + str(proxy[0x2])
    proxy_dict = {'https': "https://" + proxy_str }

    try:
        r = requests.get(url, headers=headers, proxies=proxy_dict)

        if r.status_code == 200:
            return {"id": proxy.id, "code": r.status_code}

    except ProxyError:
        return {"id": proxy.id, "code": -1}

    except SSLError:
        return {"id": proxy.id, "code": -2}

    except ConnectionError:
        return {"id": proxy.id, "code": -3}


def deploy(data_conn):
    engine = sqlalchemy.create_engine("postgresql+psycopg2://" + data_conn["user"] + ":" + data_conn["password"] + "@" +  data_conn["host"] + "/" + data_conn["db"])

    try:
        with engine.connect() as conn:
            ips_dropear_list = []
            ips_work_list = []

            meta = sqlalchemy.schema.MetaData()
            meta.reflect(bind=conn)
            proxy_table = "selenium_1_proxys"
            meta_tabla = meta.tables[proxy_table]

            result = conn.execute("SELECT id,addr,port FROM " + proxy_table)
            url = 'https://en-gb.facebook.com/login/identify/?ctx=recover&ars=facebook_login&from_login_screen=0'

            headers = {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
                "Accept-Language": "en-US,en;q=0.5",
                "DNT": "1",
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1"
            }

            with futures.ThreadPoolExecutor(max_workers=2) as executor:
                future_test_results = [executor.submit(check_proxy, url, one_record, headers) for one_record in result]

                for future_test_result in future_test_results:
                    result = future_test_result.result()

                    if result["code"] == 200:
                        ips_dropear_list.append(result)
                        print("works " + result["id"])

                    else:
                        qry = sqlalchemy.delete(meta_tabla).where(meta_tabla.c.id)
                        result = conn.execute(qry, {"id": result["id"]})
                        print("delete " + result["id"])

    except sqlalchemy.exc.OperationalError:
        print("sqlalchemy.exc.OperationalError")


def conn_db(data_conn):
    engine = sqlalchemy.create_engine(
        "postgresql+psycopg2://" + data_conn["user"] + ":" + data_conn["password"] + "@" + data_conn["host"] + "/" + data_conn["db"])

    try:
        return engine.connect()

    except sqlalchemy.exc.OperationalError:
        return None


def get_proxy(conn):
    meta = sqlalchemy.schema.MetaData()
    meta.reflect(bind=conn)
    proxy_table = "selenium_1_proxys"
    meta_tabla = meta.tables[proxy_table]

    return conn.execute("SELECT id,addr,port FROM " + proxy_table)


def delete_proxy(conn):
    meta = sqlalchemy.schema.MetaData()
    meta.reflect(bind=conn)
    proxy_table = "selenium_1_proxys"
    meta_tabla = meta.tables[proxy_table]

    stmt = sqlalchemy.delete(meta_tabla).where(meta_tabla.c.id == 1952)
    result = conn.execute(stmt, {"ìd": 1952})


conn_obj = conn_db(data_conn)
# get_proxy(conn_obj)
delete_proxy(conn_obj)
# deploy(data_conn)
