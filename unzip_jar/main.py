import glob
import zipfile
import os
from pathlib import Path
import shutil

find_dir = ["H:\\programfiles\\xampp", "G:"]
output_dir = "G:\\jar_output\\"
target_class = ["JMSAppender.class", "JndiLookup.class"]


def zipdir(path, ziph):
    for root, dirs, files in os.walk(path):
        for file in files:
            prm1 = os.path.join(root, file)
            prm2 = os.path.relpath(os.path.join(root, file), path)
            ziph.write(prm1, prm2)


def check_jar(file_path, output_tmp_folder, class_to_remove):
    try:
        with zipfile.ZipFile(file_path, 'r') as zip_file:
            for c_file in zip_file.infolist():
                for c_class in class_to_remove:
                    if c_file.filename.find(c_class) > 0x0:
                        print("[+] Found " + c_class + " on " + file_path)

                        # create dir
                        filename = Path(file_path).stem
                        final_dir = output_tmp_folder + "\\" + filename + "\\"
                        os.mkdir(final_dir)

                        # create dir and copy original .jar
                        original_jar_dir = final_dir + "original"
                        os.mkdir(original_jar_dir)
                        base_name = os.path.basename(file_path)
                        original_jar_dir_with_ext = original_jar_dir + "\\" + base_name
                        shutil.copyfile(file_path, original_jar_dir_with_ext)

                        # create dir
                        original_jar_mod_dir = final_dir + "mod"
                        os.mkdir(original_jar_mod_dir)

                        # create dir
                        original_jar_mod_unzip_dir = original_jar_mod_dir + "\\unzip"
                        os.mkdir(original_jar_mod_unzip_dir)

                        # create dir
                        original_jar_new_jar_dir = original_jar_mod_dir + "\\new_jar"
                        os.mkdir(original_jar_new_jar_dir)
                        jar_patched_file_name = original_jar_new_jar_dir + "\\" + base_name

                        # find vuln .class
                        zip_file.extractall(original_jar_mod_unzip_dir)
                        dir_find = original_jar_mod_unzip_dir + "\\**\\" + c_class
                        class_found = glob.glob(dir_find, recursive=True)

                        # remove vuln .class
                        for file_to_delete in class_found:
                            print("[+] Removing " + file_to_delete)
                            os.remove(file_to_delete)

                        # create zip/jar
                        new_jar = zipfile.ZipFile(jar_patched_file_name, 'w', zipfile.ZIP_DEFLATED)
                        zipdir(original_jar_mod_unzip_dir, new_jar)
                        new_jar.close()


    except FileNotFoundError:
        print("[!] FileNotFoundError")


def deploy(path_to_find, path_to_output, t_class):
    for c_dir in path_to_find:
        files = glob.glob(c_dir + "\\**\\*.jar", recursive=True)

        for c_file in files:
            check_jar(c_file, path_to_output, t_class)


deploy(find_dir, output_dir, target_class)