from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.firefox.options import FirefoxProfile
from selenium.common.exceptions import ElementClickInterceptedException
import threading
import time


url = "https://www.amd.com/es/support/previous-drivers/graphics/amd-radeon-r9-series/amd-radeon-r9-300-series/amd-radeon-r9-390"
engine_file_path = "D:\\movibles\\11-15-20\\geckodriver-v0.28.0-win64\\geckodriver.exe"


def download_amd():
    options = FirefoxProfile()
    options.set_preference("browser.download.dir", "G:\\movibles2\\")
    options.set_preference("pdfjs.disabled", True)
    options.set_preference("pdfjs.enabledCache.state", False)
    options.set_preference("browser.download.folderList", 2)
    options.set_preference("browser.download.useDownloadDir", True)
    options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream")
    wdriv = webdriver.Firefox(executable_path=engine_file_path, firefox_profile=options)
    flag = 0x1

    while(flag):
        try:
            print("[+] Conectando ...")
            ret_get = wdriv.get(url)

            if ret_get is not None:
                print(ret_get.status_code)

            else:
                flag = 0

        except WebDriverException:
            print("[!] WebDriverException exception")
            flag = 2
            time.sleep(10)

    summ = wdriv.find_element_by_xpath("//details[@class='os-group']")
    summ.click()

    time.sleep(10)

    try:
        summ.find_element_by_xpath("//a[@class='btn-transparent-black']").click()

    except ElementClickInterceptedException:
        print("[!] ElementClickInterceptedException")
        wdriv.close()
        return -1

    time.sleep(20)
    wdriv.get("about:downloads")

    while(True):

        if wdriv.page_source.find(" left ") > 0x0:
            print("[+] Downloading..")

        else:
            print("[!] ERROR")
            wdriv.close()
            return -1

        time.sleep(10)


def get_status_download():
    url = "http://192.168.1.183/example.html"
    options = FirefoxProfile()
    options.set_preference("browser.download.dir", "P:\\movibles")
    options.set_preference("pdfjs.disabled", True)
    options.set_preference("pdfjs.enabledCache.state", False)
    options.set_preference("browser.download.folderList", 2)
    options.set_preference("browser.download.useDownloadDir", True)
    options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream")
    # options.update_preferences()
    wdriv = webdriver.Firefox(executable_path=engine_file_path, firefox_profile=options)
    flag = 0x1

    while(flag):
        try:
            print("[+] Conectando ...")
            ret_get = wdriv.get(url)

            if ret_get is not None:
                print(ret_get.status_code)

            else:
                flag = 0

        except WebDriverException:
            print("[!] WebDriverException exception")
            flag = 2
            time.sleep(10)

    # richlistbox = wdriv.find_element_by_id('downloadsRichListBox')
    # richlistitem = richlistbox.find_element_by_xpath("//richlistitem[@class='download download-state']")
    # print(richlistitem.get_attribute("state"))

ret = download_amd()
while(ret == -1):
    ret = download_amd()

# get_status_download()
