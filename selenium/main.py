from selenium import webdriver
from selenium.common.exceptions import TimeoutException, WebDriverException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time
import threading


def proxy_chrome():
    proxy = {'address': '123.123.123.123:2345',
             'usernmae': 'johnsmith123',
             'password': 'iliketurtles'}

    capabilities = dict(DesiredCapabilities.CHROME)
    capabilities['proxy'] = {'proxyType': 'MANUAL',
                                'httpProxy': proxy['address'],
                                'ftpProxy': proxy['address'],
                                'sslProxy': proxy['address'],
                                'noProxy': '',
                                'class': "org.openqa.selenium.Proxy",
                                'autodetect': False}

    driver = webdriver.Chrome(executable_path="M:\\tools\\selenium-engines\\geckodriver-v0.31.0-win64\\chromedriver.exe", desired_capabilities=capabilities)
    driver.get("https://google.com")


def get_req(url):
    wdriv = webdriver.Firefox(executable_path="M:\\tools\\selenium-engines\\geckodriver-v0.32.2\\geckodriver.exe")
    flag = 1

    while(flag):
        try:
            print("[+] Conectando ...")            
            ret_get = wdriv.get(url)

            if ret_get is not None:
                print(ret_get.status_code)

            else:
                flag = 0

        except WebDriverException:
            print("[!] WebDriverException exception")
            flag = 2
            time.sleep(10)    


def load_urls():

    urls = [

    "https://www.mrwve.com/",
    "https://medium.com/analytics-vidhya/algorithmic-trading-in-python-macd-ca508c0017b",
    "https://towardsdatascience.com/algorithmic-trading-with-macd-and-python-fef3d013e9f3",
    "https://medium.com/codex/algorithmic-trading-with-macd-in-python-1c2769a6ad1b",
    "https://preview.themeforest.net/item/isida-plastic-surgery-clinic-medical-wordpress-theme/full_screen_preview/18911053"

    ]

    for url in urls:
        thrd = threading.Thread(target=get_req, args=[url])
        thrd.run()


def find_pelis():

    url_targets = [

    "https://gdrivelatinohd.net/?s="

    ]

    with open("J:\\shared\\search-pelis.txt") as fp:
        pelis_all_lines = fp.readlines()

        for url in url_targets:
            for peli in pelis_all_lines:
                if len(peli) > 0x2:
                    final_url = url + peli
                    get_req(final_url)
                    time.sleep(60)


find_pelis()
# load_urls()
# load_urls()
# proxy_ff()
# proxy_chrome()
