from os import listdir
from os.path import isfile, join

my_path = "L:\\movibles\\videos\\enlancetv"
only_files = [f for f in listdir(my_path) if isfile(join(my_path, f))]
print(only_files)

with open("list.txt", "w") as fp:
    for one_file in only_files:
        fp.write("file '" + one_file + "'\n")

    fp.close()
