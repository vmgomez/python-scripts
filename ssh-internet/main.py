import paramiko
import time
from socket import gaierror
import datetime


def ssh_loop(host, prt, usr, pwd, cmd):
    repeat = True

    while repeat:
        client = paramiko.client.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.load_system_host_keys()
        time.sleep(1)

        try:
            client.connect(host, port=prt, username=usr, password=pwd)

        # except OSError:
        #    print("[!] OSError")
        #    continue

        except gaierror:
            print("[!] gaierror")
            continue

        except ConnectionError:
            print("[!] ConnectionError")
            continue

        except TimeoutError:
            print("[!] TimeoutError")
            continue

        except EOFError:
            print("[!] ERROR")
            continue

        except paramiko.ssh_exception.SSHException:
            print("[!] paramiko.ssh_exception.SSHException")
            continue

        except paramiko.ssh_exception.AuthenticationException:
            print("[!] ERROR")
            continue

        except paramiko.ssh_exception.NoValidConnectionsError:
            print("[!] ERROR")
            continue

        try:
            stdin, stdout, stderr = client.exec_command(cmd)

        except ConnectionResetError:
            print("[!] exec_command() ERROR")
            continue

        except paramiko.ssh_exception.SSHException:
            print("[!] SSHException ERROR")
            continue

        str_output = ''.join(str(e) for e in stdout)
        str_err_output = ''.join(str(e) for e in stderr)
        client.close()
        return {"stdout": str_output, "stderr": str_err_output}

        repeat = False


cmd = "ping 8.8.8.8"

hst = "192.168.1.3"
port = 8123
usr = "root"
pwd = "pxgtsjmnt"


def check_modem_internet():
    ping_txt = ssh_loop(hst, port, usr, pwd, cmd)

    now = str(datetime.datetime.now())

    if ping_txt["stdout"].find("icmp_seq=") > -1:
        print(now + " Hay internet")
        print("stdout " + ping_txt["stdout"])
        print("stderr " + ping_txt["stderr"])
        return 0x0

    if len(ping_txt["stderr"]) == 0x0:
        print(now + " Habia internet")
        print("stdout " + ping_txt["stdout"])
        print("stderr " + ping_txt["stderr"])
        return 0x0

    if ping_txt["stderr"].find("Network unreachable") > 0x0:
        print(now + " No hay!")
        return 0


while True:
    check_modem_internet()
    time.sleep(40)