import json
import requests
from requests.exceptions import ProxyError, SSLError, ConnectionError
from concurrent import futures


def check_proxy(url, headers, proxy):
    proxy_dict = {'https': "https://" + proxy}

    try:
        r = requests.get(url, headers=headers, proxies=proxy_dict)

        if r.status_code == 200:
            print("[+] " + proxy + " SUCCESS.!")
            return r.status_code

    except ProxyError:
        # print("[!] ProxyError on " + proxy)
        return -1

    except SSLError:
        # print("[!] SSLError on " + proxy)
        return -2

    except ConnectionError:
        # print("[!] ConnectionError on " + proxy)
        return -3


def deploy():
    with open("proxies-http+https-beautify.json", "r") as fp:
        proxys = json.load(fp)

        url = 'https://facebook.com'

        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0",
            "Accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
            "Accept-Language": "en-US,en;q=0.5",
            "DNT": "1",
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": "1"
        }

        with futures.ThreadPoolExecutor(max_workers=50) as executor:
            future_test_results = [executor.submit(check_proxy, url, headers, proxy) for proxy in proxys["https"]]

            for future_test_result in future_test_results:
                test_result = future_test_result.result()


deploy()
