from sqlalchemy import create_engine
import pandas as pd
from datetime import datetime
from datetime import timedelta
from collections import Counter


def gen_common_names(df, out, count_repeat, lst_append):

    lst_nams = []
    common_nams = []

    for c_name in df['pnombre']:
        lst_nams.append(c_name)

    dict_nams = Counter(lst_nams)
    
    for key in dict_nams:
        if(dict_nams[key] > count_repeat):
            for c_append in lst_append:
                data = key + c_append

                if(len(data) > 7):
                    data_lower = key.lower() + c_append
                    data_cap = key.capitalize() + c_append
                    out.write(data + "\n")
                    out.write(data_cap + "\n")
                    out.write(data_lower + "\n")


def gen_join_two(df, out, f_out=False, capitalizeFirst=False, capitalizeSecond=False, lowerSecond=False, capitalizeBoth=False, lowerFirst=False):
    df_2 = df
    ret = []

    for c_name in df['pnombre']:
        p1 = c_name
        for c_name2 in df_2['pnombre']:
            p2 = c_name2

            if lowerFirst is True:
                p1 = c_name.lower()

            if capitalizeSecond is True:
                p2 = c_name2.capitalize()

            if lowerSecond is True:
                p2 = c_name2.lower()

            n_join = p1 + p2
            out.write(n_join + "\n")



def dump_file(dats, output):            

    for c_line in dats:
        output.write(c_line + "\n")


def gen_single_string(df, output):

    for c_name in df['pnombre']:
        lst_mayus_mins = mayus_permut(c_name)

    return lst_mayus_mins

        # mayus-mins iterator
        # for c_line in lst_mayus_mins:
        #    output.write(c_line + "\n")

def gen_single_last(df,
                    lst_dates,
                    output,
                    mayus=True,
                    first_mayus=True,
                    lower=True):

    # names iterator
    for c_name in df['pnombre']:
        lst_mayus_mins = mayus_permut(c_name)

        # mayus-mins iterator
        for c_mayus_mins in lst_mayus_mins:

            # dates iterator
            for c_date in lst_dates:
                final_str = c_mayus_mins + c_date
                output.write(final_str + "\n")


def gen_date(total_days, init_date):
    str_init_date = datetime.strptime(init_date, "%m%d%y")
    ret_dates = []

    for c_day in range(total_days):
        end_date = str_init_date + timedelta(days=c_day)
        ret_dates.append(end_date.strftime("%d/%m/%y"))
        ret_dates.append(end_date.strftime("%d%m%y"))
        ret_dates.append(end_date.strftime("%-d%-m%y"))

    return ret_dates


def get_data(qry):

    str_conn_wm = 'mysql://usr_cne:oIuoGtf5vf6v7@localhost/cne'
    str_conn_wifislax = 'mysql://usr_cne:oIuoGtf5vf6v7@localhost/cne?unix_socket=/var/run/mysql/mysql.sock'

    engine = create_engine(str_conn_wifislax, echo=False)
    df = pd.read_sql_query(qry, engine)
    return df


# nombre
# NOMBRE
# Nombre
def mayus_permut(c_names, mayus=True, first_mayus=True, lower=True):

    ret_lst = []

    if lower is True:
        ret_lst.append(c_names.lower())

    if mayus is True:
        ret_lst.append(c_names.upper())

    if first_mayus is True:
        ret_lst.append(c_names.capitalize())

    return ret_lst


def gen_single_mayus(data,
                  output,
                  mayus=True,
                  lower=True,
                  first_mayus=True):
    lst_names = []

    # iterator names
    for c_name in data['pnombre']:
        lst_names = mayus_permut(c_name, mayus, first_mayus, lower)

        # lower, mayus, capitalize iterator
        for c_lst_name in lst_names:
            output.write(c_lst_name + "\n")


def gen_mayus_int(data,
                  limit,
                  output,
                  mayus=True,
                  lower=True,
                  first_mayus=True):

    lst_names = []

    # iterator names
    for c_name in data['pnombre']:
        lst_names = mayus_permut(c_name, mayus, first_mayus, lower)

        # lower, mayus, capitalize iterator
        for c_lst_name in lst_names:

            # range iterator
            for c_numb in range(limit):
                final_str = c_lst_name + str(c_numb)
                output.write(final_str + "\n")


def gen_mid_last(data,
                 mid,
                 last,
                 output,
                 mayus=True,
                 lower=True,
                 first_mayus=True):

    for c_names in data['pnombre']:
        for c_mid in mid:
            for c_last in last:

                if lower is True:
                    cadena = c_names.lower() + c_mid + c_last
                    output.write(cadena + "\n")

                if first_mayus is True:
                    cadena = c_names.capitalize() + c_mid + c_last
                    output.write(cadena + "\n")


def gen_single_join(data,
                    signo,
                    output,
                    mayus=True,
                    lower=True,
                    first_mayus=True):

    nams = data['pnombre']

    for c_names in data['pnombre']:
        for c_names_other in nams:
            for c_signs in signo:

                if lower is True:
                    cadena = c_names.lower() + c_signs + c_names_other.lower()
                    output.write(cadena + "\n")

                if mayus is True:
                    cadena = c_names.upper() + c_signs + c_names_other.upper()
                    output.write(cadena + "\n")

                if first_mayus is True:
                    cadena = c_names.capitalize() + c_signs + c_names_other.lower()
                    output.write(cadena + "\n")


def main():

    #lst_dates = gen_date(21900, "010160")

    print("[+] Get Data from MySQL")
    str_qry = 'SELECT pnombre FROM dats'
    # str_qry = 'SELECT DISTINCT(pnombre) FROM dats'
    # str_qry = 'SELECT DISTINCT(pnombre) FROM dats WHERE LENGTH(pnombre) > 7'
    df = get_data(str_qry)

    print("[+] Creating dictionary file")

    fp_wifislax = "/mnt/sda3/generic_rule.dict"
    fp_wm = "/mnt/sda2/no-ta.dict"
    f_out = open(fp_wifislax, "w")

    anade = ["123", "1234", "321", "1234", "4321", "123.", "1234.", "1", "12"]
    gen_common_names(df, f_out, 50, anade)
    return



    # nombreSegundoNombre
    gen_join_two(df, f_out, lowerFirst=True, capitalizeSecond=True)
    #dump_file(data, f_out)
    return

    # nombre
    # Nombre
    # NOMBRE
    gen_single_string(df, f_out)


    # gen_single_mayus(df, f_out)

    # Le anade una fecha al final
    gen_single_last(df, lst_dates, output=f_out)

    # Nombre
    # nombre123
    # nombre321
    # NOMBRE111
    gen_mayus_int(df, 9999, output=f_out)
    #return 


    # nombre.nombre
    # nombre nombre
    # nombre_nombre
    # nombre@nombre
    # nombre-nombre
    # nombrenombre
    # medio = [""]
    # gen_single_join(df, medio, f_out, first_mayus=False, mayus=False)


    # nombreilove
    # nombre.ilove
    # nombre@ilove
    # nombre-ilove
    # nombre_ilove
    # medio = ["-", "@", "", ".", " ", "_", "y", "and"]
    # last=["ilove", "avenger", "music", "avengers", "lol", "vive", "elpollo", "plc", "teama", "dj",
    #      "tecoje", "directv", "gammer", "cojeperras", "gigolo", "elcojeperras", "elgigolo",
    #      "laperra", "papaoso", "mamaosa", "gamer", "jodete", "mamamelo", "aire", "aires",
    #      "melomama", "123", "conoetumadre", "ilove", "elsapo",
    #      "cojevieja", "elcojevieja", "elcojeviejas", "elperrote",
    #      "elpapi", "dealer", "cs", "csgo", "dota", "eldealer", "love", "elposho", "quierepipe", "19cm",
    #      "elhampon", "elhampa", "hampa", "xxx", "merwebo", "caraetabla", "bna", "elbigboss", "eltombo",
    #      "elpran", "singador", "sabrosito", "elsabrosito",
    #      "sabrocito", "elsabrocito", "lostieneloco", "mamalo", "mamalox"]

    # gen_mid_last(df, mid=medio, last=last, output=f_out)


main()

