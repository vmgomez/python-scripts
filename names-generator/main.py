from sqlalchemy import create_engine
import pandas as pd
from datetime import datetime
from datetime import timedelta


def gen_two_concat(df, concat_1, concat_2, output, capitalizeFirst=True, lower=True, ):
    ret = []
    
    for c_name in df['pnombre']:
        for c_concat_2 in concat_2:
            a = c_name + concat_1 + c_concat_2
            ret.append(a)

            if capitalizeFirst is True:
                ret.append(a.capitalize())

            if lower is True:
                ret.append(a.lower())

    for c_ret in ret:
        output.write(c_ret + "\n")


def gen_str_h4ck3r(df, map_chars, output, capitalizeFirst=True, lower=True):
        a = 0
        ret = []


        for c_name in df['pnombre']:
            ret.append(c_name)

            if capitalizeFirst is True:
                ret.append(c_name.capitalize())

            if lower is True:
                c_name = c_name.lower()
                ret.append(c_name)

            copy_input = c_name
            all_mods = c_name

            for c_char in copy_input:
                    a = a + 1                    
                    for c_map in map_chars:                            
                            if c_map['t'] == c_char:
                                    c1 = copy_input[:a].replace(c_map['t'], c_map['r']) + copy_input[a:]
                                    all_mods = all_mods[:a].replace(c_map['t'], c_map['r']) + all_mods[a:]                                    
                                    ret.append(c1)                                    

                                    if c1 != all_mods:
                                            ret.append(all_mods)

                                    if capitalizeFirst is True:
                                        c1_cap = c1.capitalize()

                                    if c1_cap != c1:
                                        ret.append(c1_cap)

            # dates iterator
            for c_ret in ret:
                output.write(c_ret + "\n")



def gen_join_two(df, f_out=False, capitalizeFirst=False, capitalizeSecond=False, capitalizeBoth=False):
    df_2 = df

    for c_name in df['pnombre']:
        for c_name2 in df_2['pnombre']:
            n_join = c_name + c_name2
            print(n_join)



def gen_single_string(df, output):

    for c_name in df['pnombre']:
        lst_mayus_mins = mayus_permut(c_name)

    return lst_mayus_mins

        # mayus-mins iterator
        # for c_line in lst_mayus_mins:
        #    output.write(c_line + "\n")

def gen_single_last(df,
                    lst_dates,
                    output,
                    mayus=True,
                    first_mayus=True,
                    lower=True):

    # names iterator
    for c_name in df['pnombre']:
        lst_mayus_mins = mayus_permut(c_name)

        # mayus-mins iterator
        for c_mayus_mins in lst_mayus_mins:

            # dates iterator
            for c_date in lst_dates:
                final_str = c_mayus_mins + c_date
                output.write(final_str + "\n")


def gen_date(total_days, init_date):
    str_init_date = datetime.strptime(init_date, "%m%d%y")
    ret_dates = []

    for c_day in range(total_days):
        end_date = str_init_date + timedelta(days=c_day)
        ret_dates.append(end_date.strftime("%d/%m/%y"))
        ret_dates.append(end_date.strftime("%d%m%y"))
        ret_dates.append(end_date.strftime("%-d%-m%y"))

    return ret_dates


def get_data(qry):

    str_conn_wm = 'mysql://usr_cne:oIuoGtf5vf6v7@localhost/cne'
    str_conn_wifislax = 'mysql://usr_cne:oIuoGtf5vf6v7@localhost/cne?unix_socket=/var/run/mysql/mysql.sock'

    engine = create_engine(str_conn_wifislax, echo=False)
    df = pd.read_sql_query(qry, engine)
    return df


# nombre
# NOMBRE
# Nombre
def mayus_permut(c_names, mayus=True, first_mayus=True, lower=True):

    ret_lst = []

    if lower is True:
        ret_lst.append(c_names.lower())

    if mayus is True:
        ret_lst.append(c_names.upper())

    if first_mayus is True:
        ret_lst.append(c_names.capitalize())

    return ret_lst


def gen_single_mayus(data,
                  output,
                  mayus=True,
                  lower=True,
                  first_mayus=True):
    lst_names = []

    # iterator names
    for c_name in data['pnombre']:
        lst_names = mayus_permut(c_name, mayus, first_mayus, lower)

        # lower, mayus, capitalize iterator
        for c_lst_name in lst_names:
            output.write(c_lst_name + "\n")


def gen_mayus_int(data,
                  limit,
                  output,
                  mayus=True,
                  lower=True,
                  first_mayus=True):

    lst_names = []

    # iterator names
    for c_name in data['pnombre']:
        lst_names = mayus_permut(c_name, mayus, first_mayus, lower)

        # lower, mayus, capitalize iterator
        for c_lst_name in lst_names:

            # range iterator
            for c_numb in range(limit):
                final_str = c_lst_name + str(c_numb)
                output.write(final_str + "\n")


def gen_mid_last(data,
                 mid,
                 last,
                 output,
                 mayus=True,
                 lower=True,
                 first_mayus=True):

    for c_names in data['pnombre']:
        for c_mid in mid:
            for c_last in last:

                if lower is True:
                    cadena = c_names.lower() + c_mid + c_last
                    output.write(cadena + "\n")

                if first_mayus is True:
                    cadena = c_names.capitalize() + c_mid + c_last
                    output.write(cadena + "\n")


def gen_single_join(data,
                    signo,
                    output,
                    mayus=True,
                    lower=True,
                    first_mayus=True):

    nams = data['pnombre']

    for c_names in data['pnombre']:
        for c_names_other in nams:
            for c_signs in signo:

                if lower is True:
                    cadena = c_names.lower() + c_signs + c_names_other.lower()
                    output.write(cadena + "\n")

                if mayus is True:
                    cadena = c_names.upper() + c_signs + c_names_other.upper()
                    output.write(cadena + "\n")

                if first_mayus is True:
                    cadena = c_names.capitalize() + c_signs + c_names_other.lower()
                    output.write(cadena + "\n")


def main():

    #lst_dates = gen_date(21900, "010160")

    # print("[+] Get Data from MySQL")
    # str_qry = 'SELECT DISTINCT(pnombre) FROM dats'
    str_qry = 'SELECT pnombre FROM dats'
    # str_qry = 'SELECT DISTINCT(pnombre) FROM dats WHERE LENGTH(pnombre) > 7'
    # str_qry = 'SELECT DISTINCT(pnombre) FROM dats WHERE LENGTH(pnombre) > 2'
    df = get_data(str_qry)
    dups = df.pivot_table(index=['pnombre'], aggfunc='size')

    for c_dups in dups:
        if c_dups > 0x50:
            print(dups[c_dups])

    exit(0)

    
    # file_name = "/mnt/sdb3/movibles/df_names.csv"
    # df.to_csv(file_name, sep='\t')
    return

    print("[+] Creating dictionary file")

    fp_wifislax = "/mnt/sdb3/movibles/generic_rule.dict"
    fp_wm = "/mnt/sda2/no-ta.dict"
    f_out = open(fp_wifislax, "w")

    map_replacer = [{'t': 'l', 'r': '1'},
    {'t': 'i', 'r': '1'},
    {'t': 'e', 'r': '3'},
    {'t': 'a', 'r': '4'},
    {'t': 's', 'r': '5'},
    {'t': 'g', 'r': '6'},
    {'t': 't', 'r': '7'},
    {'t': 'o', 'r': '0'}]

    # two_concat = ["123", "321", "111", "6969"]
    # gen_two_concat(df, ".", two_concat, f_out)


    # ret = gen_str_h4ck3r(df, map_replacer, f_out)

    # gen_join_two(df)
    # return

    # nombre
    # Nombre
    # NOMBRE
    # gen_single_string(df, f_out)


    # gen_single_mayus(df, f_out)

    # Le anade una fecha al final
    # gen_single_last(df, lst_dates, output=f_out)

    # Nombre
    # nombre123
    # nombre321
    # NOMBRE111
    # gen_mayus_int(df, 9999, output=f_out)
    # return 


    # nombre.nombre
    # nombre nombre
    # nombre_nombre
    # nombre@nombre
    # nombre-nombre
    # nombrenombre
    medio = [""]
    gen_single_join(df, medio, f_out, first_mayus=False, mayus=False)


    # nombreilove
    # nombre.ilove
    # nombre@ilove
    # nombre-ilove
    # nombre_ilove
    # medio = ["-", "@", "", ".", " ", "_", "y", "and"]
    # last=["ilove", "avenger", "music", "avengers", "lol", "vive", "elpollo", "plc", "teama", "dj",
    #      "tecoje", "directv", "gammer", "cojeperras", "gigolo", "elcojeperras", "elgigolo",
    #      "laperra", "papaoso", "mamaosa", "gamer", "jodete", "mamamelo", "aire", "aires",
    #      "melomama", "123", "conoetumadre", "ilove", "elsapo",
    #      "cojevieja", "elcojevieja", "elcojeviejas", "elperrote",
    #      "elpapi", "dealer", "cs", "csgo", "dota", "eldealer", "love", "elposho", "quierepipe", "19cm",
    #      "elhampon", "elhampa", "hampa", "xxx", "merwebo", "caraetabla", "bna", "elbigboss", "eltombo",
    #      "elpran", "singador", "sabrosito", "elsabrosito",
    #      "sabrocito", "elsabrocito", "lostieneloco", "mamalo", "mamalox"]

    # gen_mid_last(df, mid=medio, last=last, output=f_out)


main()

